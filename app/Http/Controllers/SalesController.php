<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sales;

class SalesController extends Controller
{
    
    /**
     * index
     *
     * @return void
     */
    public function index(){
        return view('upload-file');
    }

        
    /**
     * sales
     *
     * @param  mixed $request
     * @return void
     */
    public function sales(Request $request){
        if($request->has('mycsv')){
            
            $data = array_map('str_getcsv', file($request->mycsv));

            //Chosir la premier ligne qui sert d entete
            $header = $data[0];
    
            //Enlever l entete
            unset($data[0]);

            foreach($data as $item){
                //format a value
                $saleData = array_combine($header, $item);
                Sales::create($saleData);
            }

            return $header;
    
            //foreach ($)
        }
        return request()->all();
    }
}
